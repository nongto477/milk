<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('username', 100);
            $table->string('password', 100);
            $table->string('email', 100);
            $table->string('firstname', 100);
            $table->string('lastname', 100);
            $table->bigInteger('manager_point');
            $table->bigInteger('manager_point_wallet');
            $table->bigInteger('direct_point');
            $table->bigInteger('direct_point_wallet');
            $table->bigInteger('status_direct_id')->unsigned()->index();;
            $table->foreign('status_direct_id')->references('id')->on('status_direct')->onDelete('cascade');
            $table->boolean('agency');
            $table->bigInteger('superior_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
