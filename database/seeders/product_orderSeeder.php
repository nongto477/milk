<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\product_order;

class product_orderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        product_order::factory()->count(10)->create();
    }
}
