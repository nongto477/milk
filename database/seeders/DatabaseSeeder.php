<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            status_directSeeder::class,
            status_orderSeeder::class,
            customersSeeder::class,
            productsSeeder::class,
            ordersSeeder::class,
            product_orderSeeder::class,
        ]);
    }
}
