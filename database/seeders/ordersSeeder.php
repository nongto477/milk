<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\orders;

class ordersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        orders::factory()->count(10)->create();
    }
}
