<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\customers;

class customersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        customers::create(
            [
                'username' => 'kaito',
                'password' => 'kaito',
                'email' => 'kaito@gmail.com',
                'firstname' => 'kaito',
                'lastname' => 'ryouga',
                'manager_point' => 0,
                'manager_point_wallet' => 1000000,
                'direct_point' => 0,
                'direct_point_wallet' => 1000000,
                'status_direct_id' => 1,
                'agency' => true,
                'superior_id' => 0,
            ]
        );

        customers::factory()->count(20)->create();
    }
}
