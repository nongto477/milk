<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\products;

class productsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        products::factory()->count(10)->create();
    }
}
