<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\status_order;

class status_orderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        status_order::create(
            ['name' => 'chờ duyệt'], 
            ['name' => 'được duyệt']
        );
    }
}
