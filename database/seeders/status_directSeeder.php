<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\status_direct;

class status_directSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        status_direct::create(
            ['name' => 'chờ xét duyệt'], 
            ['name' => 'xét duyệt'],
            ['name' => 'đã chuyển tiền']
        );
    }
}
