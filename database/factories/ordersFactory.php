<?php

namespace Database\Factories;

use App\Models\orders;
use App\Models\customers;
use App\Models\status_order;
use Illuminate\Database\Eloquent\Factories\Factory;

class ordersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = orders::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => customers::all()->random()->id,
            'status_order_id' => status_order::all()->random()->id,
            'total' => $this->faker->numberBetween(1, 1000000)
        ];
    }
}
