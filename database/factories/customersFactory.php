<?php

namespace Database\Factories;

use App\Models\customers;
use App\Models\status_direct;
use Illuminate\Database\Eloquent\Factories\Factory;

class customersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = customers::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $id = 2;
        $superior_id = $this->faker->numberBetween(1, 20);

        while ($id == $superior_id) {
            $superior_id = $this->faker->numberBetween(1, 20);
        }

        return [
            'id' => $id++,
            'username' => $this->faker->unique()->name(),
            'password' => '123456',
            'email' => $this->faker->unique()->safeEmail(),
            'firstname' => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
            'manager_point' => $this->faker->numberBetween(1, 1000000),
            'manager_point_wallet' => $this->faker->numberBetween(1, 1000000),
            'direct_point' => $this->faker->numberBetween(1, 1000000),
            'direct_point_wallet' => $this->faker->numberBetween(1, 1000000),
            'status_direct_id' => status_direct::all()->random()->id,
            'agency' => $this->faker->boolean(),
            'superior_id' => $superior_id,
        ];
    }
}
