<?php

namespace Database\Factories;

use App\Models\product_order;
use App\Models\products;
use App\Models\orders;
use Illuminate\Database\Eloquent\Factories\Factory;

class product_orderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = product_order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => products::all()->random()->id,
            'order_id' => orders::all()->random()->id,
            'quantity' => $this->faker->numberBetween(1, 10),
        ];
    }
}
