<?php

namespace Database\Factories;

use App\Models\products;
use Illuminate\Database\Eloquent\Factories\Factory;

class productsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = products::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->name(),
            'thumbnail' => $this->faker->image('public/storage/images',640,480, null, false),
            'price' => $this->faker->numberBetween(1, 1000000),
            'quantity' => $this->faker->numberBetween(1, 100),
        ];
    }
}
