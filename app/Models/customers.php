<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class customers extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'username',
        'password',
        'email',
        'firstname',
        'lastname',
        'manager_point',
        'manager_point_wallet',
        'direct_point',
        'direct_point_wallet',
        'status_direct_id',
        'agency',
        'superior_id'
    ];
    protected $table = "customers";
}
