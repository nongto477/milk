#/bin/bash

if [ "$@" = "hot" ]; then
    ./vendor/bin/sail npm run watch-poll
else
    ./vendor/bin/sail $@
fi