$(document).ready(function(){

	$("ul.sb_list_down").parent().append(""); //Only shows drop down trigger when js is enabled - Adds empty span tag after ul.subnav
	
	$("ul.sb_list li p").click(function() { //When trigger is clicked...
		
		//Following events are applied to the subnav itself (moving subnav up and down)
		if($(this).hasClass('sb_list_show'))$(this).parent().find("ul.sb_list_down").slideUp('_default').slideUp('slow');
		else $(this).parent().find("ul.sb_list_down").slideDown('_default').show();

		$(this).parent().click(function() {
		}, function(){	
			if($(this).hasClass('sb_list_show'))$(this).parent().find("ul.sb_list_down").slideUp('slow');
			else $(this).parent().find("ul.sb_list_down").slideUp('slow');
		});

		//Following events are applied to the trigger (Hover events for the trigger)
		}).click(function() { 
			if($(this).hasClass('sb_list_show')){
				$(this).removeClass("sb_list_show");
				$(this).addClass("sb_list");
				}
			else{ $(this).addClass("sb_list_show");
				$(this).removeClass("sb_list");
			}//On hover over, add class "subhover"
		});
		
	// header menu down	
	$("ul.hm_down").parent().append("<a></a>"); //Only shows drop down trigger when js is enabled - Adds empty span tag after ul.subnav
	
	$("li.hm_down p").hover(function() { //When trigger is clicked...
		
		//Following events are applied to the subnav itself (moving subnav up and down)
		$(this).parent().find("ul.hm_down").slideDown('_default').show(); //Drop down the subnav on click

		$(this).parent().hover(function() {
		}, function(){	
			$(this).parent().find("ul.hm_down").slideUp('slow'); //When the mouse hovers out of the subnav, move it back up
		});

		//Following events are applied to the trigger (Hover events for the trigger)
		})
});